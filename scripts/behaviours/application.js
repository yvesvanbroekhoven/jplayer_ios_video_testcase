var $           = require('jquery')
,   window      = require('browser/window')
,   document    = require('browser/document')
,   navigator   = require('browser/navigator')
;

require('jquery-jplayer');

/*
 * DOM Ready
 */
$(function(){
  console.info('DOM ready');
  
  $("#jquery_jplayer_1").jPlayer({
    ready: function () {
      $(this).jPlayer("setMedia", {
        m4v: "/kurt-stallaert.m4v"
      });
      $(this).click(function(){
        $(this).jPlayer("play");
      })
      
    },
    swfPath: "/swf",
    supplied: "m4v",
    solution: "html, flash",
    errorAlerts: true
  }); 
  
});
